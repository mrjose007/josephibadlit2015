var Post = React.createClass({
  render: function() {
    var style = {
      backgroundColor: this.props.bgcolor
    }
    return (
      <div style={style}>
        {this.props.author}: {this.props.children}
      </div>
    );
  }
});

var PostForm = React.createClass({
    render: function() {
      return <p>And I will become a form for posts!</p>
    }
});

var PostList = React.createClass({
  render: function() {
    var postNodes = this.props.data.map(function (comment, index) {
      return (
        <Post key={index} author={comment.author}>
          {comment.post}
        </Post>
      );
    });
    return (
      <div>
        {postNodes}
      </div>
    );
  }
});

var PostBox = React.createClass({

  render: function() {
    return (
      <div>
        <PostList data={this.props.data}/>
        <PostForm />
      </div>
    );
  }

});