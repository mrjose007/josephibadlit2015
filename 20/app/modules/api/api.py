from flask import Blueprint
from flask import jsonify
from flask import request

mod = Blueprint('api', __name__)

sample_data = [
        {'author':'computer', 'post':'10000', 'date':'October 22 2015', 'gender': 'Not Specified'},
        {'author':'pizza', 'post':'500', 'gender':'Female'},
        {'author':'apple', 'post':'30', 'gender': 'Not Specified'},
    ]

@mod.route('/getsampledata', methods=['GET','POST'])
def get_sample_data():
    print request.form.keys()
    if request.method=='POST':
        sample_data.append({
            'author':request.form['author'], 
            'post':request.form['post'], 
        })
    return jsonify(posts=sample_data)