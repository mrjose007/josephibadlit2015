from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)

@app.route('/')
def hello_world():
	return 'Hello World!'

@app.route('/<username>', methods=['GET', 'POST'])
def hello_username(username):

	if request.method== 'POST':
		for x in request.args:
			print x + ' ' + str(request.args[x])
		return render_template('hello.html',name=username)
	elif request.method=='GET':
		for x in request.args:
			print x + ' ' + str(request.args[x])
		return render_template('hello.html',name=username)
	return 'Not Supported'

@app.route('/about')
def about_page():
	return 'This is the about page!'

if __name__ == '__main__':
	app.run()